	function Sinit() {
		// CANVAS SET UP
		container = document.getElementById('Sdiv');
		document.body.appendChild(container);
		container.appendChild(Scanvas);
		Scanvas.width = SCREEN_WIDTH;
		Scanvas.height = SCREEN_HEIGHT;
		starsImage.src = Sgraphic;
	}
	
	function Sloop() {
		// make some particles
		if(starMouseDown) {
			makeSParticle(8);
		} else {
			makeSParticle(4);
			// clear the canvas
			Scontext.globalCompositeOperation = 'source-in';
			Scontext.fillStyle="rgba(0,0,0,0.2)";
			Scontext.fillRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

			// iterate through each particle
			for (i=0; i<Sparticles.length; i++) {
				var Sparticle = Sparticles[i]; 
				// render it
				Sparticle.render(Scontext); 
				// and then update. We always render first so particle
				// appears in the starting point.
				Sparticle.update();
			}
		}
		// Keep taking the oldest particles away until we have 
		// fewer than the maximum allowed.
		while(Sparticles.length>sMAX_PARTICLES)
		Sparticles.shift();
	}

	function makeSParticle(particleCount) {
		for(var i=0; i<particleCount;i++) {
			// create a new particle in the middle of the stage
			//var FWparticle = new fireworksParticle(fireworksImage, 850, 200);
			var posX = Math.ceil(Math.random()*1440)*20;
			var posY = Math.ceil(Math.random()*650);
			var Sparticle = new starsParticle(starsImage, posX, posY);
			// give it a random x and y velocity
			Sparticle.velX = randomRange(-15,15);
			Sparticle.velY = randomRange(-15,15);
			Sparticle.size = randomRange(0.1,0.9);
			Sparticle.gravity = Sgravity; 
			Sparticle.drag = Sfriction;
			Sparticle.shrink = Sshrink; 
			// sets the blend mode so particles are drawn with an additive blend
			Sparticle.compositeOperation = 'lighter';
			// add it to the array
			Sparticles.push(Sparticle); 	
		}
	}