	var body = document.querySelector("body");
	function FWinit() {
		// CANVAS SET UP
		container = document.getElementById('FWdiv');
		document.body.appendChild(container);
		container.appendChild(FWcanvas);
		FWcanvas.width = SCREEN_WIDTH;
		FWcanvas.height = SCREEN_HEIGHT;
		fireworksImage.src = FWgraphic;
		body.addEventListener("click", function() {
			makeFWParticle(200);
		}, false);
	}
	
	function FWloop() {
		FWcontext.globalCompositeOperation = 'source-in';
		FWcontext.fillStyle="rgba(0,0,0,0.2)";
		FWcontext.fillRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
		// iterate through each particle
		for (i=0; i<FWparticles.length; i++) {
			var FWparticle = FWparticles[i]; 
			// render it
			FWparticle.render(FWcontext); 
			// and then update. We always render first so particle
			// appears in the starting point.
			FWparticle.update();
		}
		// Keep taking the oldest particles away until we have 
		// fewer than the maximum allowed.
		while(FWparticles.length>fwMAX_PARTICLES) {
			FWparticles.shift();
		}
	}

	function makeFWParticle(particleCount) {
		for(var i=0; i<particleCount;i++) {
			// create a new particle in the middle of the stage
			var FWparticle = new fireworksParticle(fireworksImage, 850, 200);
			var posX = event.clientX;
			var posY = event.clientY;
			var FWparticle = new fireworksParticle(fireworksImage, posX, posY);
			// give it a random x and y velocity
			FWparticle.velX = randomRange(-30,30);
			FWparticle.velY = randomRange(-30,30);
			FWparticle.size = randomRange(0.9,1);
			FWparticle.gravity = FWgravity; 
			FWparticle.drag = FWfriction;
			FWparticle.shrink = FWshrink; 
			// sets the blend mode so particles are drawn with an additive blend
			FWparticle.compositeOperation = 'lighter';
			// add it to the array
			FWparticles.push(FWparticle); 	
		}
	}
	
	