	var SCREEN_WIDTH = window.innerWidth;
	var SCREEN_HEIGHT = window.innerHeight;
	var HALF_WIDTH = window.innerWidth / 2;
	var HALF_HEIGHT = window.innerHeight / 2;
	
	var FFcanvas = document.getElementById('fireflies');
	var FFcontext = FFcanvas.getContext('2d');

	var ffMouseDown = false;
	var FFparticles = [];
	var ffMAX_PARTICLES = 12;
	var FFgravity = 0;
	var FFfriction = 0.5;
	var FFshrink = 0.99; //changes the size of the particles as time progresses
	var FFgraphic = 'images/firefly.png';

	firefliesImage = new Image();
	FFinit();
	setInterval(FFloop, 200); //100 is 1 second
