	var SCREEN_WIDTH = window.innerWidth;
	var SCREEN_HEIGHT = window.innerHeight
	var HALF_WIDTH = window.innerWidth / 2;
	var HALF_HEIGHT = window.innerHeight / 2;

	var FWcanvas = document.getElementById('fireworks');
	var FWcontext = FWcanvas.getContext('2d');

	var FWparticles = [];
	var fwMAX_PARTICLES = 1000;
	var FWgravity = 0.7;
	var FWfriction = 0.9;
	var FWshrink = 0.9; //changes the size of the particles as time progresses
	var FWgraphic = 'images/fireworks.png';
	
	fireworksImage = new Image();
	FWinit();
	setInterval(FWloop, 30); //100 is 1 second
