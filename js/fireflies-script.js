	function FFinit() {
		// CANVAS SET UP
		container = document.createElement('FFdiv');
		document.body.appendChild(container);
		container.appendChild(FFcanvas);
		FFcanvas.width = SCREEN_WIDTH;
		FFcanvas.height = SCREEN_HEIGHT;
		firefliesImage.src = FFgraphic;
	}
	
	function FFloop() {
		// make some particles
		if(ffMouseDown)
			makeFFParticle(8);
		else
			makeFFParticle(4);
			// clear the canvas
			FFcontext.globalCompositeOperation = 'source-in';
			FFcontext.fillStyle="rgba(0,0,0,0.2)";
			FFcontext.fillRect(0,0, SCREEN_WIDTH, SCREEN_HEIGHT);
			// iterate through each particle
		for (i=0; i<FFparticles.length; i++) {
			var FFparticle = FFparticles[i]; 
			// render it
			FFparticle.render(FFcontext); 
			// and then update. We always render first so particle
			// appears in the starting point.
			FFparticle.update();
		}
		// Keep taking the oldest particles away until we have 
		// fewer than the maximum allowed.
		while(FFparticles.length>ffMAX_PARTICLES)
		FFparticles.shift(); 
	}

	function makeFFParticle(particleCount) {
		for(var i=0; i<particleCount;i++) {
			// create a new particle at a random x location
			var posX = Math.ceil(Math.random()*1440)*20;
			var posY = Math.ceil(Math.random()*650);
			var FFparticle = new firefliesParticle(firefliesImage, posX, posY);
			// give it a random x and y velocity
			FFparticle.velX = randomRange(-15,15);
			FFparticle.velY = randomRange(-15,15);
			FFparticle.size = randomRange(1,2);
			FFparticle.gravity = FFgravity; 
			FFparticle.drag = FFfriction;
			FFparticle.shrink = FFshrink; 
			// sets the blend mode so particles are drawn with an additive blend
			FFparticle.compositeOperation = 'lighter';
			// add it to the array
			FFparticles.push(FFparticle); 	
		}
	}